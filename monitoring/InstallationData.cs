﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using MySql;
using System.Configuration;

namespace Monitoring
{
    public class InstallationData
    {
        //constucture
        public InstallationData(String installationId, System.DateTime lastResetTime)
        {
            _installationID = installationId;
            _lastResetTime = lastResetTime;
        }

        private string _connectionStr = ConfigurationManager.ConnectionStrings["MyDBConnection"].ConnectionString;
        private string _tableName = "installation_data";
        private string _installationID;

        private DateTime _lastResetTime;

        private long _sgbTimer;

        private double _voltage;
        private double _current;
        private double _windSpeed;
        private double _ilim;
        //will be deleted
        private double _elv;
        private double _eli;

        public int lastIdAdded;
        //private TurbineData[] _turbines = new TurbineData[20];

        public string InstallationID
        {
            get
            {
                return _installationID;
            }
            set
            {
                _installationID = value;
            }
        }

        public System.DateTime LastResetTime
        {
            get
            {
                return _lastResetTime;//.ToUniversalTime(); //ToUniversalTime().ToString("yyyy/MM/dd HH:mm:ss");
            }
            set
            {
                _lastResetTime = value;//.ToUniversalTime();//.ToString("yyyy/MM/dd HH:mm:ss");
            }
        }


        public long SGBTimer
        {
            get
            {

                return _sgbTimer;
            }
            set
            {

                _sgbTimer = value;
            }
        }
       
        public double WindSpeed
        {
            get
            {
                return _windSpeed;
            }
            set
            {
                _windSpeed = value;
            }
        }

        public double Voltage
        {
            get
            {
                return _voltage;
            }
            set
            {
                _voltage = value;
            }
        }

        public double Current
        {
            get
            {
                return _current;
            }
            set
            {
                _current = value;
            }
        }

        public double Ilim
        {
            get
            {
                return _ilim;
            }
            set
            {
                _ilim = value;
            }
        }
        //will be deleted
        public double Elv
        {
            get
            {
                return _elv;
            }
            set
            {
                _elv = value;
            }
        }
        //will be deleted
        public double Eli
        {
            get
            {
                return _eli;
            }
            set
            {
                _eli = value;
            }
        }

        //public TurbineData[] Turbines
        //{
        //    get
        //    {
        //        return _turbines;
        //    }
        //    set
        //    {
        //        _turbines = value;
        //    }
        //}

        //TurbineData[] turbines = new TurbineData[20];

        public bool Insert()//(TurbineData[] myturbines)
        {
            bool blnResult = true;

            MySqlConnection _myconn = new MySqlConnection(_connectionStr);

            MySqlCommand _command;

            try
            {
                //dbConnection.OpenConnection();
                _myconn.Open();
                
                //string query = "insert into " + _tableName + "(`idinstallation_data`,`time`, `milliSec`,`v`,`i`,`ws`,`ilim`,`elv`,`eli`) VALUES( '" + InstallationID + "' ,'" + Time.ToString("yyyy/MM/dd HH:mm:ss") + "', '" + MilliSec + "', " + VBattery + ", " + Current + ", " + WindSpeed + ", " + Ilim + ", " + Elv + ", " + Eli + ")";
                string query = "insert into " + _tableName + "(`idinstallation_data`,`timer`,`lastResetTime`,`v`,`i`,`ws`,`ilim`) VALUES( '" + InstallationID + "' ," + SGBTimer + " ,'" + LastResetTime.ToString("yyyy/MM/dd HH:mm:ss") + "', " + Voltage + ", " + Current + ", " + WindSpeed + ", " + Ilim + ")";
                //(`id`,
                _command = new MySqlCommand(query, _myconn);
                _command.ExecuteNonQuery();
                _command = new MySqlCommand("SELECT LAST_INSERT_ID() FROM installation_data",_myconn);
                lastIdAdded = int.Parse(_command.ExecuteScalar().ToString());

                //for (int i = 0; i < Turbines.Length && Turbines[i] != null ; i++)
                //{
                //    Turbines[i].InstallationId = lastIdAdded;
                //    Turbines[i].Time = System.DateTime.Now;
                //    Turbines[i].Insert();
                //}

                _command.Dispose();
            }
            catch (System.Exception ex)
            {
                blnResult = false;
                PublicMethods.logToDB("Exception in inserting InstallationData",ex.ToString(),InstallationID);
            }
            finally
            {
                if (_myconn.State != System.Data.ConnectionState.Closed)
                    _myconn.Close();
                _myconn.Dispose();
            }
            return (blnResult);
        }
    }
}
