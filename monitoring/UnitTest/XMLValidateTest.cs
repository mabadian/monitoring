﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Monitoring;

namespace UnitTest
{
    [TestFixture]
    class XMLValidateTest
    {
        /// <summary>
        /// init is a setup Test method that is executing before any test method
        /// </summary>
        [SetUp()]
        public void init()
        {

        }

        /// <summary>
        /// Test is a wellformated xml file is valid
        /// </summary>
        [Test()]
        public void ValidationTest1()
        {
            string str = @"<measurz>
            <sgb time='11:59' ws='24' v='36' i='20' ilim='20' />
                <nodes>
                    <node sn='(100)00000083' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                    <node sn='serial_number_n+1' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                </nodes>
            </measurz>";

            Assert.IsTrue(Parser.IsValidXML(str));
        }

        /// <summary>
        /// misspelling tag.
        /// the start tag is not the same tag as close tag. 
        /// measurz2 instead of measurz first line
        /// </summary>
        [Test()]
        public void ValidationTest2()
        {
            string str = @"<measurz2>
            <sgb time='11:59' ws='24' v='36' i='20' ilim='20' />
                <nodes>
                    <node sn='(100)00000083' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                    <node sn='serial_number_n+1' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                </nodes>
            </measurz>";

            Assert.IsFalse(Parser.IsValidXML(str));
        }

        /// <summary>
        /// sn attribute with no value
        /// </summary>
        [Test()]
        public void ValidationTest3()
        {
            string str = @"<measurz>
            <sgb time='11:59' ws='24' v='36' i='20' ilim='20' />
                <nodes>
                    <node sn= >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                    <node sn='(100)00000083' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                </nodes>
            </measurz>";

            Assert.IsFalse(Parser.IsValidXML(str));
        }

        /// <summary>
        /// where the = is missing between attribute and the value
        /// vin'input_voltage' in line 5
        /// </summary>
        [Test()]
        public void ValidationTest4()
        {
            string str = @"<measurz>
            <sgb time='11:59' ws='24' v='36' i='20' ilim='20' />
                <nodes>
                    <node sn='(100)00000083' >
                        <data seq='0' vin'11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                    <node sn='serial_number_n+1' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                </nodes>
            </measurz>";

            Assert.IsFalse(Parser.IsValidXML(str));
        }

        /// <summary>
        /// Not having closing tag
        ///  last </nodes> tag
        /// </summary>
        [Test()]
        public void ValidationTest5()
        {
            string str = @"<measurz>
            <sgb time='11:59' ws='24' v='36' i='20' ilim='20' />
                <nodes>
                    <node sn='(100)00000083' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                    <node sn='serial_number_n+1' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
            </measurz>";

            Assert.IsFalse(Parser.IsValidXML(str));
        }

        /// <summary>
        /// not having closing tag 
        /// the data tag  "<data" is never closed in the seq1 last node
        /// </summary>
        [Test()]
        public void ValidationTest6()
        {
            string str = @"<measurz>
            <sgb time='11:59' ws='24' v='36' i='20' ilim='20' />
                <nodes>
                    <node sn='(100)00000083' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                    <node sn='serial_number_n+1' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                </nodes>
            </measurz>";

            Assert.IsFalse(Parser.IsValidXML(str));
        }

        /// <summary>
        /// we miss an attribute and the value not making the XML invalid, so we expect having IsValidXML.
        /// But we do check this in the parsing
        /// we are missing the v='voltage' in the data tag.
        /// </summary>
        [Test()]
        public void ValidationTest7()
        {
            string str = @"<measurz>
            <sgb time='11:59' ws='24' v='36' i='20' ilim='20' />
                <nodes>
                    <node sn='(100)00000083' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' i='59' st='status'/>
                    </node>
                    <node sn='serial_number_n+1' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                </nodes>
            </measurz>";

            Assert.IsTrue(Parser.IsValidXML(str));
        }

        /// <summary>
        /// not close the value with "'"  so it mess up the rest
        /// i='1 st='OK' instead of i='1' st='OK' in the sgb tag
        /// </summary>
        [Test()]
        public void ValidationTest8()
        {
            string str = @"<measurz>
            <sgb time='11:59' ws='24' v='36' i='20' ilim='20' />
                <nodes>
                    <node sn='(100)00000083' >
                        <data seq='0' vin='11' vout='111' v='1' i='1 st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                    <node sn='serial_number_n+1' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                </nodes>
            </measurz>";

            Assert.IsFalse(Parser.IsValidXML(str));
        }

        /// <summary>
        /// missing the name of the data tag!
        /// line 11
        /// </summary>
        [Test()]
        public void ValidationTest9()
        {
            string str = @"<measurz>
            <sgb time='11:59' ws='24' v='36' i='20' ilim='20' />
                <nodes>
                    <node sn='(100)00000083' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                    <node sn='serial_number_n+1' >
                        <seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                </nodes>
            </measurz>";

            Assert.IsFalse(Parser.IsValidXML(str));
        }
    }
}
