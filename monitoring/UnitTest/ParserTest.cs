﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Monitoring;
using MySql.Data.MySqlClient;
using MySql;
using System.Configuration;

namespace UnitTest
{
    [TestFixture]
    public class ParserTest
    {
        
        string tblInstallationData = "installation_data";
        string tblTurbineData = "turbine_data";
        string tblLostPackets = "log_lost_packets";
        string tblError = "log_error";
        int beforeInstalDataAdd = 0;
        int beforeTurbineDataAdd = 0;
        int beforeLostDataAdd = 0;
        int beforeErrorLogAdd = 0;

        /// <summary>
        /// init is a setup Test method that is executing before any test method
        /// </summary>
        [SetUp()]
        public void init()
        {
            PublicTestMethods.DeleteData(tblInstallationData);
            PublicTestMethods.DeleteData(tblTurbineData);
            PublicTestMethods.DeleteData(tblError);
            PublicTestMethods.DeleteData(tblLostPackets);

            beforeInstalDataAdd = PublicTestMethods.RowCounter(tblInstallationData);
            beforeTurbineDataAdd = PublicTestMethods.RowCounter(tblTurbineData);
            beforeLostDataAdd = PublicTestMethods.RowCounter(tblLostPackets);
            beforeErrorLogAdd = PublicTestMethods.RowCounter(tblError);
        }


        [Test()]
        public void ParseTestSinglePacket()
        {
            string str = 
            @"<measurz>
            <sgb time='11:59' ws='24' v='36' i='20' ilim='20' />
                <nodes>
                    <node sn='(100)00000083' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                    <node sn='serial_number_n+1' >
                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                    </node>
                </nodes>
            </measurz>";

            Parser.Parse(str, "TEST");

            int afterAdd = PublicTestMethods.RowCounter(tblInstallationData);

            Assert.AreEqual(beforeInstalDataAdd + 1, afterAdd);
        }

        //[Test()]
        //public void ParseTest2GoodAttachedPacket()
        //{
        //    string s1 = "V=1.1,I=1.1,WS=1.1,ILIM=1,ELV=10,ELI=11\r\nV=2.2,I=22,WS=0.2,ILIM=20,ELV=2,ELI=22\r\n";
        //    Parser.Parse(s1, "TEST");

        //    int afterAdd = RowCounter(tblInstallationData);

        //    Assert.AreEqual(beforeInstalDataAdd + 2, afterAdd);
        //}

        ///// <summary>
        ///// Test 2Good1BadAttachedPacket- bad packet is long 
        ///// </summary>
        //[Test()]
        //public void ParseTest2Good1BadAttachedPacket()
        //{
        //    string s1 = "V=1.1,I=1.1,WS=1.1,ILIM=1,ELV=10,ELI=11.1\r\nV=2.1,I=1.1,WS=1.1,ILIM=1,ELV=10,ELI=2222\r\nV=3.1,I=1.1,WS=3.1,ILIM=3,ELV=10,ELI=33\r\n";
        //    Parser.Parse(s1, "TEST");

        //    int afterAdd = RowCounter(tblInstallationData);
        //    int afterErrorLogAdd = RowCounter(tblError);
        //    Assert.AreEqual(beforeInstalDataAdd + 2, afterAdd);
        //    Assert.AreEqual(beforeErrorLogAdd + 1, afterErrorLogAdd);
        //}

        //[Test()]
        //public void ParseTest3GoodAttachedPacket()
        //{
        //    string s1 = "V=1.1,I=1.1,WS=1.1,ILIM=1,ELV=10,ELI=11\r\nV=2.2,I=22,WS=0.2,ILIM=20,ELV=2,ELI=22\r\nV=3.1,I=1.1,WS=3.1,ILIM=3,ELV=10,ELI=33\r\n";
        //    Parser.Parse(s1, "TEST");

        //    int afterAdd = RowCounter(tblInstallationData);

        //    Assert.AreEqual(beforeInstalDataAdd + 3, afterAdd);
        //}

        //[Test()]
        //public void ParseTest1Good1BadAttachedPacket()
        //{
        //    string s1 = "V=1.1,I=1.1,WS=1.1,ILIM=1,ELV=10,ELI=BAD1\r\nV=2.2,I=22,WS=0.2,ILIM=20,ELV=2,ELI=22\r\n";
        //    Parser.Parse(s1, "TEST");

        //    int afterAdd = RowCounter(tblInstallationData);
        //    int afterLostDataAdd = RowCounter(tblLostPackets);
        //    Assert.AreEqual(beforeInstalDataAdd + 1, afterAdd);
        //    //Assert.AreEqual(beforeErrorLogAdd + 1, afterErrorLogAdd);
        //}
        ///////////old tests

        //[Test()]
        //public void ParseTest3()
        //{
        //    int beforeAdd = RowCounter(tblInstallationData);
        //    int beforeLog = RowCounter(lostPacketsTable);

        //    string s1 = "V=0.4,I=40,WS=44,ILIM=44,ELV=44,ELI=04\r\nV=5,I=5.1,WS=5.5,ILIM=5.0,ELV=5,ELI=05\r\nV=06,I=0.6,WS=0.6,6";
        //    Parser.Parse(s1, "111");

        //    int afterAdd = RowCounter(tblInstallationData);
        //    int afterLog = RowCounter(lostPacketsTable);

        //    Assert.AreEqual(beforeAdd + 2, afterAdd);
        //    Assert.AreEqual(beforeLog + 1, afterLog);
        //}

        //[Test()]
        //public void ParseTest4()
        //{
        //    int beforeAdd = RowCounter(tblInstallationData);
        //    int beforeLog = RowCounter(lostPacketsTable);

        //    string s1 = "V=0.7,I=7.1,WS=0.7,ILIM=7.77,ELV=7,ELI=07\r\nV=0.8,I=0.8,WS=0.8,ILIM=0.8,ELV=8,ELI=08\r\nV=0.9,I=0.9,WS=0.9,9\r\n";
        //    Parser.Parse(s1, "This is Test");

        //    int afterAdd = RowCounter(tblInstallationData);
        //    int afterLog = RowCounter(lostPacketsTable);

        //    Assert.AreEqual(beforeAdd + 2, afterAdd);
        //    Assert.AreEqual(beforeLog + 1, afterLog);
        //}


    }
}
