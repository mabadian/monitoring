﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Monitoring;
using System.Configuration;

namespace UnitTest
{
    [TestFixture]
    class TurbineInsertTest
    {
        string tblInstallationData = "installation_data";
        string tblTurbineData = "turbine_data";
        string tblError = "log_error";
        int beforeInstalDataAdd = 0;
        int beforeTurbineDataAdd = 0;
        int beforeErrorLogAdd = 0;

        [SetUp()]
        public void Setup()
        {
            PublicTestMethods.DeleteData(tblInstallationData);
            PublicTestMethods.DeleteData(tblTurbineData);
            PublicTestMethods.DeleteData(tblError);

            beforeInstalDataAdd = PublicTestMethods.RowCounter(tblInstallationData);
            beforeTurbineDataAdd = PublicTestMethods.RowCounter(tblTurbineData);
            beforeErrorLogAdd = PublicTestMethods.RowCounter(tblError);
        }

        [Test()]
        public void TestInsert1()
        {
            InstallationData n = new InstallationData("IdtttttttttttttTestttttttttttttt");
            n.I = 23;
            n.Voltage = 32;
            n.WindSpeed = 24;
            n.Time = System.DateTime.Now;
            n.Turbines[0] = new TurbineData("TestTurbineID11");
            n.Turbines[1] = new TurbineData("TestTurbineID22");
            n.Turbines[2] = new TurbineData("TestTurbineID33");
            n.Turbines[3] = new TurbineData("TestTurbineID44");
            n.Insert();

            int afterInstallAdd = PublicTestMethods.RowCounter(tblInstallationData);
            int afterTurbineAdd = PublicTestMethods.RowCounter(tblTurbineData);

            Assert.AreEqual(beforeInstalDataAdd + 1, afterInstallAdd);
            Assert.AreEqual(beforeTurbineDataAdd + 4, afterTurbineAdd);
        }
    }
}
