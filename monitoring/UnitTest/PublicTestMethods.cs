﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql;
using MySql.Data.MySqlClient;
using System.Configuration;
namespace UnitTest
{

    static class PublicTestMethods
    {
        static string _connectionstr = ConfigurationManager.ConnectionStrings["MyDBConnection"].ConnectionString;

        public static int RowCounter(string tableName)
        {
            MySqlConnection myconn = new MySqlConnection(_connectionstr);//"server=localhost;database=monitoring_test;userid=root;password=123456");//ConfigurationSettings.AppSettings["ConnectionString"]);
            MySqlCommand command;
            int count = 0;

            try
            {

                myconn.Open();
                //Check whether the Drop Down has existing items. If YES, empty it.

                string query = "select count(id) from " + tableName;
                command = new MySqlCommand(query, myconn);
                count = int.Parse(command.ExecuteScalar().ToString());
                command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
                if (myconn.State != System.Data.ConnectionState.Closed)
                    myconn.Close();
                myconn.Dispose();
            }
            return count;
        }

        public static void DeleteData(string tableName)
        {
            MySqlConnection myconn = new MySqlConnection(_connectionstr);
            MySqlCommand command;

            try
            {
                //dbConnection.OpenConnection();
                myconn.Open();
                //Check whether the Drop Down has existing items. If YES, empty it.

                //string query = "delete from " + tableName + "where version=0";
                string query = "truncate table " + tableName;
                command = new MySqlCommand(query, myconn);
                command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
                if (myconn.State != System.Data.ConnectionState.Closed)
                    myconn.Close();
                myconn.Dispose();
            }
        }
    }
}
