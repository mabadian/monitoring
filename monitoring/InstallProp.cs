﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Monitoring
{
    class InstallProp
    {
        private string _installationID;
        private DateTime _resetTime;
        private string _ipAddress;
        private int _port;


        public string InstallationID
        {
            get
            {
                return _installationID;
            }
            set
            {
                _installationID = value;
            }
        }

        public System.DateTime ResetTime
        {
            get
            {
                return _resetTime;//.ToUniversalTime(); //ToUniversalTime().ToString("yyyy/MM/dd HH:mm:ss");
            }
            set
            {
                _resetTime = value;//.ToUniversalTime();//.ToString("yyyy/MM/dd HH:mm:ss");
            }
        }


        public string IpAddress
        {
            get
            {

                return _ipAddress;
            }
            set
            {

                _ipAddress = value;
            }
        }

        public int Port
        {
            get
            {

                return _port;
            }
            set
            {

                _port = value;
            }
        }

        public static InstallProp[] GetPrperties(string DGE)
        {
            InstallProp[] ins = null;
            string _myConnectionString = "";
            int installCounter = 0;
            MySqlConnection _connection = null;
            MySqlCommand _command = null;

            _myConnectionString = ConfigurationManager.ConnectionStrings["MyDBConnection"].ConnectionString;//"server=localhost;database=monitoring_test;userid=root;password=123456";// ConfigurationManager.ConnectionStrings["MyDBConnection"].ConnectionString;
            _connection = new MySqlConnection(_myConnectionString);

            try
            {
                _connection.Open();
                _command = new MySqlCommand("select count(*) from installation_ip_port where DGE = '" + DGE + "';", _connection);// where DGE#=" + DGE + ";",_connection);

                installCounter = int.Parse(_command.ExecuteScalar().ToString());

                _command = _connection.CreateCommand();
                _command.CommandText = "select * from installation_ip_port where DGE= '" + DGE + "'";

                //Or
                //_command = new MySqlCommand("select * from installation_ip_port where DGE= " + DGE + ";");

                MySqlDataReader Reader;

                Reader = _command.ExecuteReader();
                ins = new InstallProp[installCounter];
                int counter = 0;
                while (Reader.Read())
                {
                    ins[counter] = new InstallProp();
                    string row = "";
                    int i;
                    for (i = 0; i < Reader.FieldCount; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                if (Reader.GetName(i) == "idInstallation_data")
                                {
                                    ins[counter].InstallationID = (string)Reader.GetValue(i);
                                    row += Reader.GetValue(i).ToString() + "\r\n ";
                                }

                                break;
                            case 1:
                                if (Reader.GetName(i) == "ipAddress")
                                {
                                    ins[counter].IpAddress = (string)Reader.GetValue(i);
                                    row += Reader.GetValue(i).ToString() + "\r\n ";
                                }
                                break;
                            case 2:
                                if (Reader.GetName(i) == "port")
                                {
                                    ins[counter].Port = (int)Reader.GetValue(i);
                                    row += Reader.GetValue(i).ToString() + "\r\n ";
                                }
                                break;
                            case 3:
                                if (Reader.GetName(i) == "resetTime")
                                {
                                    ins[counter].ResetTime = (DateTime)Reader.GetValue(i);
                                    row += Reader.GetValue(i).ToString() + "\r\n ";
                                }
                                break;
                        }
                    }
                    counter++;
                    Console.WriteLine(row);
                }
            }
            catch (Exception ex)
            {
                PublicMethods.logToDB(ex.Message, "Unexpected error happening when get list of installationId, Ip Address, Port and ResetTime from Database related to the current DGE", "DGE#:'" + DGE +"'");
            }
            finally
            {
                if (_connection.State != System.Data.ConnectionState.Closed)
                {
                    _connection.Close();
                }
                _command.Dispose();
            }
            return ins;
        }
    }
}
