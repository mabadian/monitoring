﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using MySql;
using System.Configuration;

namespace Monitoring
{
    public class InstallationData
    {
        //constucture
        public InstallationData(String installationId)
        {
            _installationID = installationId; 
        }

        private String _connectionStr = ConfigurationManager.ConnectionStrings["MyDBConnection"].ConnectionString;
        private String _tableName = "installation_data";
        private String _installationID;
        private DateTime _time = System.DateTime.Now;
        private int _milliSec;

        private double _voltage;
        private double _i;
        private double _windSpeed;
        private double _ilim;
        //will be deleted
        private double _elv;
        private double _eli;

        private TurbineData[] _turbines = new TurbineData[20];


        public string InstallationID
        {
            get
            {
                return _installationID;
            }
            set
            {
                _installationID = value;
            }
        }

        public System.DateTime Time
        {
            get
            {
                return _time;//.ToUniversalTime(); //ToUniversalTime().ToString("yyyy/MM/dd HH:mm:ss");
            }
            set
            {
                _time = value;//.ToUniversalTime();//.ToString("yyyy/MM/dd HH:mm:ss");
            }
        }

        public int MilliSec
        {
            get
            {

                return _time.Millisecond;
            }
            set
            {

                _milliSec = _time.Millisecond;
            }
        }
       
        public double WindSpeed
        {
            get
            {
                return _windSpeed;
            }
            set
            {
                _windSpeed = value;
            }
        }

        public double Voltage
        {
            get
            {
                return _voltage;
            }
            set
            {
                _voltage = value;
            }
        }

        public double I
        {
            get
            {
                return _i;
            }
            set
            {
                _i = value;
            }
        }

        public double Ilim
        {
            get
            {
                return _ilim;
            }
            set
            {
                _ilim = value;
            }
        }
        //will be deleted
        public double Elv
        {
            get
            {
                return _elv;
            }
            set
            {
                _elv = value;
            }
        }
        //will be deleted
        public double Eli
        {
            get
            {
                return _eli;
            }
            set
            {
                _eli = value;
            }
        }

        public TurbineData[] Turbines
        {
            get
            {
                return _turbines;
            }
            set
            {
                _turbines = value;
            }
        }

        TurbineData[] turbines = new TurbineData[20];

        public bool Insert()//(TurbineData[] myturbines)
        {
            bool blnResult = true;

            MySqlConnection _myconn = new MySqlConnection(_connectionStr);
            MySqlCommand _command;

            try
            {
                //dbConnection.OpenConnection();
                _myconn.Open();

                //string query = "insert into " + _tableName + "(`idinstallation_data`,`time`, `milliSec`,`v`,`i`,`ws`,`ilim`,`elv`,`eli`) VALUES( '" + InstallationID + "' ,'" + Time.ToString("yyyy/MM/dd HH:mm:ss") + "', '" + MilliSec + "', " + Voltage + ", " + I + ", " + WindSpeed + ", " + Ilim + ", " + Elv + ", " + Eli + ")";
                string query = "insert into " + _tableName + "(`idinstallation_data`,`time`,`v`,`i`,`ws`,`ilim`) VALUES( '" + InstallationID + "' ,'" + Time.ToString("yyyy/MM/dd HH:mm:ss") +  "', " + Voltage + ", " + I + ", " + WindSpeed + ", " + Ilim +  ")";
                _command = new MySqlCommand(query, _myconn);
                _command.ExecuteNonQuery();
                _command = new MySqlCommand("SELECT LAST_INSERT_ID() FROM installation_data",_myconn);
                int lastIdAdded = int.Parse(_command.ExecuteScalar().ToString());

                for (int i = 0; i < Turbines.Length && Turbines[i] != null ; i++)
                {
                    Turbines[i].InstallationId = lastIdAdded;
                    Turbines[i].Time = System.DateTime.Now;
                    Turbines[i].Insert();
                }
                _command.Dispose();
            }
            catch (System.Exception ex)
            {
                blnResult = false;
                PublicMethods.logToDB("Exception in inserting InstallationData",ex.ToString(),InstallationID);
            }
            finally
            {
                if (_myconn.State != System.Data.ConnectionState.Closed)
                    _myconn.Close();
                _myconn.Dispose();
            }
            return (blnResult);
        }
    }
}
