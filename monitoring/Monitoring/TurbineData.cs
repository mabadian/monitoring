﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using MySql.Data.MySqlClient;
using MySql;

namespace Monitoring
{
    public class TurbineData
    {
        //constucture
        public TurbineData(string TurbineSerialNumber)
        {
            _serialNumber = TurbineSerialNumber; 
        }

        private string _connectionStr = ConfigurationManager.ConnectionStrings["MyDBConnection"].ConnectionString;
        private string _tableName = "turbine_data";
        // As a foreign key of the primary key ID auto incriment field in Installarioin_data table
        private Int64 _installaion_id;
        private string _serialNumber;
        private DateTime _time;

        private double _vIn;
        private double _vOut;
        private double _vBattery;
        private double _i;
        private string _status;

        /// <summary>
        /// As a foreign key of the primary key ID auto incriment field in Installarioin_data table
        /// </summary>
        public Int64 InstallationId
        {
            get
            {
                return _installaion_id;
            }
            set
            {
                _installaion_id = value;
            }
        }

        public string SerialNumber
        {
            get
            {
                return _serialNumber;
            }
            set
            {
                _serialNumber = value;
            }
        }

        public System.DateTime Time
        {
            get
            {
                return _time;//.ToUniversalTime(); //ToUniversalTime().ToString("yyyy/MM/dd HH:mm:ss");
            }
            set
            {
                _time = value;//.ToUniversalTime();//.ToString("yyyy/MM/dd HH:mm:ss");
            }
        }

        public double VIn
        {
            get
            {
                return _vIn;
            }
            set
            {
                _vIn = value;
            }
        }
        public double VOut
        {
            get
            {
                return _vOut;
            }
            set
            {
                _vOut = value;
            }
        }
        public double Voltage
        {
            get
            {
                return _vBattery;
            }
            set
            {
                _vBattery = value;
            }
        }
        public double Current
        {
            get
            {
                return _i;
            }
            set
            {
                _i = value;
            }
        }
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public bool Insert()
        {
            bool blnResult = true;

            MySqlConnection _myconn = new MySqlConnection(_connectionStr);
            MySqlCommand _command;

            try
            {
                //dbConnection.OpenConnection();
                _myconn.Open();
                //LAST_INSERT_ID() should be in the seperate field
                string query = "insert into " + _tableName + "(`installation_id`,`serial_number`,`time`,`v_in`,`v_out`,`v_battery`,`i`) VALUES( " + InstallationId + " ,'" + SerialNumber + "' ,'" + Time.ToString("yyyy/MM/dd HH:mm:ss") + "', '" + VIn + "', " + VOut + ", " + _vBattery + ", " + Current + ")";

                _command = new MySqlCommand(query, _myconn);
                _command.ExecuteNonQuery();
                _command.Dispose();
            }
            catch (System.Exception ex)
            {
                blnResult = false;
                PublicMethods.logToDB("Exception in inserting Turbine", ex.ToString(), SerialNumber);
            }
            finally
            {
                if (_myconn.State != System.Data.ConnectionState.Closed)
                    _myconn.Close();
                _myconn.Dispose();
            }
            return (blnResult);
        }
    }
}
