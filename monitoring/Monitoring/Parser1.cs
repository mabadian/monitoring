﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.IO;
//using System.Xml;
//using System.Xml.Schema;
//using System.Xml.XPath;

//namespace Monitoring
//{
//    public static class Parser1
//    {
//        static string installationID;
//        static InstallationData ins = new InstallationData("111");
//        public static void Parse(string _xmlString, string _installID)
//        {
//            bool isValid = false;
//            isValid = (_xmlString != "") && (IsValidXML(_xmlString)) && (isValidPacket(_xmlString));

//            InstallationData ins = new InstallationData(_installID);

//            //            @"<measurz>
//            //            <sgb time='11:59' ws='24' v='36' i='20' ilim='20' />
//            //                <nodes>
//            //                    <node sn='(100)00000083' >
//            //                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
//            //                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
//            //                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
//            //                    </node>
//            //                    <node sn='serial_number_n+1' >
//            //                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
//            //                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
//            //                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
//            //                    </node>
//            //                </nodes>
//            //            </measurz>";

//            using (XmlReader reader = XmlReader.Create(new StringReader(_xmlString)))
//            {
//                isValid = reader.IsStartElement("measurz");

//                if (reader.ReadToFollowing("sgb"))
//                {
//                    if (reader.MoveToFirstAttribute())
//                    {
//                        if (reader.Name == "time")
//                        {
//                            string hourMinuteDataCollected = reader.Value;
//                            System.DateTime InstallationTime = System.DateTime.Now;
//                            String[] hourMin = hourMinuteDataCollected.Split(':');
//                            string InstallHour = hourMin[0];
//                            string Installminute = hourMin[1];
//                            string InstallDate = System.DateTime.Today.ToString();
//                        }
//                        else
//                        {
//                            isValid = false;
//                        }

//                        if (reader.MoveToNextAttribute() && reader.Name == "ws")
//                        {
//                            try
//                            {
//                                ins.WindSpeed = double.Parse(reader.Value);

//                            }
//                            catch (Exception e)
//                            {
//                                isValid = false;
//                                PublicMethods.logToDB("An unexpected error accured in parsing packet WindSpeed value is not valid", e.ToString(), installationID);
//                                PublicMethods.logLostPacketToDB(_xmlString, installationID);
//                            }

//                        }
//                        else
//                        {
//                            isValid = false;
//                        }

//                        //v='voltage' i='current' ilim='set_current' />
//                        if (reader.MoveToNextAttribute() && reader.Name == "v")
//                        {
//                            try
//                            {
//                                ins.VBattery = double.Parse(reader.Value);

//                            }
//                            catch (Exception e)
//                            {
//                                isValid = false;
//                                PublicMethods.logToDB("An unexpected error accured in parsing installationPacket voltage value is not valid", e.ToString(), installationID);
//                                PublicMethods.logLostPacketToDB(_xmlString, installationID);
//                            }

//                        }
//                        else
//                        {
//                            isValid = false;
//                        }

//                        if (reader.MoveToNextAttribute() && reader.Name == "i")
//                        {
//                            try
//                            {
//                                ins.I = double.Parse(reader.Value);

//                            }
//                            catch (Exception e)
//                            {
//                                isValid = false;
//                                PublicMethods.logToDB("An unexpected error accured in parsing installationPacket current value is not valid", e.ToString(), installationID);
//                                PublicMethods.logLostPacketToDB(_xmlString, installationID);
//                            }
//                        }

//                        else
//                        {
//                            isValid = false;
//                        }

//                        if (reader.MoveToNextAttribute() && reader.Name == "ilim")
//                        {
//                            try
//                            {
//                                ins.Ilim = double.Parse(reader.Value);

//                            }
//                            catch (Exception e)
//                            {
//                                isValid = false;
//                                PublicMethods.logToDB("An unexpected error accured in parsing installationPacket ilim value is not valid", e.ToString(), installationID);
//                                PublicMethods.logLostPacketToDB(_xmlString, installationID);
//                            }
//                        }
//                    }
//                }

//                reader.ReadToFollowing("nodes");
//                //reader
//                //foreach ()
//                //for (int i = 0; i<  ; i++ )

//                int nodeCount = 0;
//                int dataCount = 0;
//                while (reader.Read())
//                {
//                    if (reader.NodeType == XmlNodeType.Element && reader.Name != "")
//                    {
//                        if (reader.NodeType == XmlNodeType.Element && reader.Name == "node")
//                        {
//                            if (reader.HasAttributes && reader.MoveToNextAttribute() && reader.Name == "sn")
//                            {
//                                //nodeCount index start from 0. it is still 0 and not added. 
//                                //Test for any type of error if value was not valid.

//                                string serialNum = reader.Value;

//                                TurbineData ins.Turbines[nodeCount] = new TurbineData(serialNum);
//                                //ins.Turbines[nodeCount] = new TurbineData(reader.Value);
//                                reader.MoveToElement();//if?! 

//                                while (reader.ReadToFollowing("data"))
//                                {
//                                    //while (reader.NodeType == XmlNodeType.Element && reader.Name == "data")
//                                    //{
//                                    //if (reader.NodeType == XmlNodeType.Element && reader.Name == "data")
//                                    //{
//                                    //here do the time & make it time of installation OR do it when we want to add it????!!!
//                                    ins.Turbines[nodeCount].Time = ins.Time;

//                                    #region Add installation Turbines
//                                    if (reader.MoveToNextAttribute() && reader.Name == "seq")
//                                    {
//                                        try
//                                        {
//                                            ins.Turbines[nodeCount].Sec = int.Parse(reader.Value);
//                                        }
//                                        catch (Exception e)
//                                        {
//                                            isValid = false;
//                                            PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket Sec value is not valid", e.ToString(), installationID);
//                                            PublicMethods.logLostPacketToDB(reader.ReadElementContentAsString(), installationID);
//                                            //OR
//                                            //PublicMethods.logLostPacketToDB(_xmlString, installationID);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        isValid = false;
//                                        PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket i value is not valid", "The xml packet does not contain st attribute in " + ins.Turbines[nodeCount].SerialNumber + " time: " + ins.Turbines[nodeCount].Time + " seq: " + ins.Turbines[nodeCount].Sec, installationID);
//                                        PublicMethods.logLostPacketToDB(reader.ReadElementContentAsString(), installationID);
//                                    }

//                                    if (reader.MoveToNextAttribute() && reader.Name == "vin")
//                                    {
//                                        try
//                                        {
//                                            ins.Turbines[nodeCount].VIn = int.Parse(reader.Value);
//                                        }
//                                        catch (Exception e)
//                                        {
//                                            isValid = false;
//                                            PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket vin value is not valid", e.ToString(), installationID);
//                                            PublicMethods.logLostPacketToDB(reader.ReadElementContentAsString(), installationID);
//                                            //OR
//                                            //PublicMethods.logLostPacketToDB(_xmlString, installationID);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        isValid = false;
//                                        PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket i value is not valid", "The xml packet does not contain st attribute in " + ins.Turbines[nodeCount].SerialNumber + " time: " + ins.Turbines[nodeCount].Time + " seq: " + ins.Turbines[nodeCount].Sec, installationID);
//                                        PublicMethods.logLostPacketToDB(reader.ReadElementContentAsString(), installationID);
//                                    }

//                                    if (reader.MoveToNextAttribute() && reader.Name == "vout")
//                                    {
//                                        try
//                                        {
//                                            ins.Turbines[nodeCount].VOut = int.Parse(reader.Value);
//                                        }
//                                        catch (Exception e)
//                                        {
//                                            isValid = false;
//                                            PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket vOut value is not valid", e.ToString(), installationID);
//                                            PublicMethods.logLostPacketToDB(reader.ReadElementContentAsString(), installationID);
//                                            //OR
//                                            //PublicMethods.logLostPacketToDB(_xmlString, installationID);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        isValid = false;
//                                        PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket i value is not valid", "The xml packet does not contain st attribute in " + ins.Turbines[nodeCount].SerialNumber + " time: " + ins.Turbines[nodeCount].Time + " seq: " + ins.Turbines[nodeCount].Sec, installationID);
//                                        PublicMethods.logLostPacketToDB(reader.ReadElementContentAsString(), installationID);
//                                    }

//                                    if (reader.MoveToNextAttribute() && reader.Name == "v")
//                                    {
//                                        try
//                                        {
//                                            ins.Turbines[nodeCount].VBattery = int.Parse(reader.Value);
//                                        }
//                                        catch (Exception e)
//                                        {
//                                            isValid = false;
//                                            PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket v value is not valid", e.ToString(), installationID);
//                                            PublicMethods.logLostPacketToDB(reader.ReadElementContentAsString(), installationID);
//                                            //OR
//                                            //PublicMethods.logLostPacketToDB(_xmlString, installationID);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        isValid = false;
//                                        PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket i value is not valid", "The xml packet does not contain st attribute in " + ins.Turbines[nodeCount].SerialNumber + " time: " + ins.Turbines[nodeCount].Time + " seq: " + ins.Turbines[nodeCount].Sec, installationID);
//                                        PublicMethods.logLostPacketToDB(reader.ReadElementContentAsString(), installationID);
//                                    }

//                                    if (reader.MoveToNextAttribute() && reader.Name == "i")
//                                    {
//                                        try
//                                        {
//                                            ins.Turbines[nodeCount].Current = int.Parse(reader.Value);
//                                        }
//                                        catch (Exception e)
//                                        {
//                                            isValid = false;
//                                            PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket i value is not valid", e.ToString(), installationID);
//                                            PublicMethods.logLostPacketToDB(reader.ReadElementContentAsString(), installationID);
//                                            //OR
//                                            //PublicMethods.logLostPacketToDB(_xmlString, installationID);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        isValid = false;
//                                        PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket i value is not valid", "The xml packet does not contain st attribute in " + ins.Turbines[nodeCount].SerialNumber + " time: " + ins.Turbines[nodeCount].Time + " seq: " + ins.Turbines[nodeCount].Sec, installationID);
//                                        PublicMethods.logLostPacketToDB(reader.ReadElementContentAsString(), installationID);
//                                    }

//                                    if (reader.MoveToNextAttribute() && reader.Name == "st")
//                                    {
//                                        ins.Turbines[nodeCount].Status = reader.Value;
//                                    }
//                                    else
//                                    {
//                                        isValid = false;
//                                        PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket i value is not valid", "The xml packet does not contain st attribute in " + ins.Turbines[nodeCount].SerialNumber + " time: " + ins.Turbines[nodeCount].Time + " seq: " + ins.Turbines[nodeCount].Sec, installationID);
//                                        PublicMethods.logLostPacketToDB(reader.ReadElementContentAsString(), installationID);
//                                    }
//                                    #endregion

//                                    dataCount++;

//                                    //HAME KAR RO ANJAM BEDE!!!!!!!!!

//                                }
//                            }
//                            nodeCount++;
//                        }
//                    }
//                }
//                reader.ReadToFollowing("nodes");
//                while (reader.Read())
//                {
//                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "data")
//                    {
//                        dataCount++;
//                    }
//                }

//                reader.ReadToFollowing("nodes");
//                for (int i = 0; i < dataCount; i++)
//                {

//                }







//                reader.ReadToFollowing("Data");
//                reader.MoveToFirstAttribute();
//                try
//                {
//                    ins.VBattery = double.Parse(reader.Value);
//                    reader.MoveToAttribute("I");
//                    ins.I = double.Parse(reader.Value);

//                    reader.MoveToAttribute("WS");
//                    ins.WindSpeed = double.Parse(reader.Value);

//                    reader.MoveToAttribute("ILIM");
//                    ins.Ilim = double.Parse(reader.Value);

//                    reader.MoveToAttribute("T");
//                    ins.Time = DateTime.Parse(System.DateTime.Now.Year + "/" + reader.Value);
//                }
//                catch
//                {
//                    isValid = false;
//                }
//                if (isValid == true)
//                {
//                    //ins.Insert();
//                }
//                //reader.ReadToFollowing("title");
//                //output.AppendLine("Content of the title element: " + reader.ReadElementContentAsString());
//            }
//        }

//        //This is method is public just for testing. it can be private after test
//        /// <summary>
//        /// check if the string is wellFormatt xml
//        /// </summary>
//        /// <param name="packet"></param>
//        /// <returns></returns>
//        public static bool IsValidXML(string xmlString)
//        {
//            bool _isValid = false;
//            try
//            {
//                using (XmlReader reader = XmlReader.Create(new StringReader(xmlString)))
//                {
//                    while (reader.Read())
//                    {
//                        _isValid = true;
//                    }
//                }
//            }
//            catch (XmlException e)
//            {
//                _isValid = false;
//            }
//            return _isValid;
//        }

//        /// <summary>
//        /// check if the string is valid Packet
//        /// </summary>
//        /// <param name="packet"></param>
//        /// <returns></returns>
//        public static bool isValidPacket(string packet)
//        {
//            bool _isValid = false;

//            //?????????? 
//            //shall i use this or keep it in the parser?!
//            //just for the sake of not being blocked in the parser class
//            _isValid = true;

//            return _isValid;
//        }
//    }
//}
