using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Net;					// Endpoint
using System.Net.Sockets;			// Socket namespace
using System.Text;                  // Text encoders
using MySql;
using System.Data.SqlClient;									

// Declare the delegate prototype to send data back to the form
delegate void AddMessage( string sNewMessage );
delegate void controler( );

namespace Monitoring
{   

	public class FormMain : System.Windows.Forms.Form
    {
        string XMLReceieved="";     
        string installationID = "";
        System.DateTime lastResetTime;

		private Socket m_sock;
        private byte[] m_byBuff = new byte[256];
		private event AddMessage m_AddMessage;
        private controler vs;

		private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.TextBox txtIpAddress;
		private System.Windows.Forms.Button btnSend;
        private TextBox textBox1;
        private TextBox textBox2;
        private CheckBox chShow;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel lblConnectStatus;
		
		private System.ComponentModel.Container components = null;

        public FormMain(String InstallationID, System.DateTime _lastResetTime)
        {
            lastResetTime = _lastResetTime;
            installationID = InstallationID;
            this.Text = installationID;
			InitializeComponent();

			// Add Message Event handler for Form decoupling from input thread
            m_AddMessage = new AddMessage(OnAddMessage);
            vs = new controler(VisibilityControl);
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.txtIpAddress = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnSend = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.chShow = new System.Windows.Forms.CheckBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblConnectStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtIpAddress
            // 
            this.txtIpAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIpAddress.Enabled = false;
            this.txtIpAddress.Location = new System.Drawing.Point(8, 14);
            this.txtIpAddress.Name = "txtIpAddress";
            this.txtIpAddress.Size = new System.Drawing.Size(1095, 20);
            this.txtIpAddress.TabIndex = 1;
            this.txtIpAddress.Text = "192.168.4.3";
            // 
            // btnConnect
            // 
            this.btnConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConnect.Location = new System.Drawing.Point(1109, 12);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(102, 23);
            this.btnConnect.TabIndex = 0;
            this.btnConnect.Text = "Connect";
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSend.Location = new System.Drawing.Point(1109, 40);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(102, 23);
            this.btnSend.TabIndex = 4;
            this.btnSend.Text = "Send Request";
            this.btnSend.Visible = false;
            //this.btnSend.Click += new System.EventHandler(this.m_btnSend_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(8, 70);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(590, 641);
            this.textBox1.TabIndex = 5;
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.Location = new System.Drawing.Point(604, 70);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox2.Size = new System.Drawing.Size(607, 641);
            this.textBox2.TabIndex = 6;
            // 
            // chShow
            // 
            this.chShow.AutoSize = true;
            this.chShow.Checked = true;
            this.chShow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chShow.Location = new System.Drawing.Point(1024, 44);
            this.chShow.Name = "chShow";
            this.chShow.Size = new System.Drawing.Size(79, 17);
            this.chShow.TabIndex = 8;
            this.chShow.Text = "Show Data";
            this.chShow.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblConnectStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 718);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1223, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblConnectStatus
            // 
            this.lblConnectStatus.Name = "lblConnectStatus";
            this.lblConnectStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // FormMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1223, 740);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.chShow);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.txtIpAddress);
            this.Controls.Add(this.btnConnect);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.FormMain_Closing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		//private void btnConnect_Click(object sender, System.EventArgs e)
        public void ClickConnect(string ipAddress, int port)
		{
			try
			{
				// Close the socket if it is still open
				if( m_sock != null && m_sock.Connected )
				{
                    ClickDisconnect();
					m_sock.Shutdown( SocketShutdown.Both );
					System.Threading.Thread.Sleep( 100 );
					m_sock.Close();
                    //if connection is open, diconnect it first and start after
                    // THIS NEEDS TO BE DONE AFTER PETER fixed the dico
                    //byte[] byteDateLine = Encoding.ASCII.GetBytes("DISCONNECT\r");
                    //m_sock.Send(byteDateLine, byteDateLine.Length, 0);
				}

				m_sock = new Socket( AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp );	

				// Define the Server address and port
                //The IP & port Comming from Database in phase2
				IPEndPoint epServer = new IPEndPoint( IPAddress.Parse( ipAddress), port );

				// Connect to the server blocking method and setup callback for recieved data
                m_sock.Connect(epServer);
                SetupRecieveCallback(m_sock);
				
				// Connect to server non-Blocking method
                //m_sock.Blocking = false;
                //AsyncCallback onconnect = new AsyncCallback(OnConnect);
                //m_sock.BeginConnect(epServer, onconnect, m_sock);
			}
			catch( Exception ex )
			{
				lblConnectStatus.Text = "Server Connect failed!" ;
                PublicMethods.logToDB("An unexpected error accured in Connection to the network", ex.ToString(), installationID);
			}
		}
        /// <summary>
        /// the method is being called in the conn
        /// </summary>
        /// <param name="ar"></param>
		public void OnConnect( IAsyncResult ar )
		{
			Socket sock = (Socket)ar.AsyncState;
			try
			{
                if (sock.Connected)
                {
                    SetupRecieveCallback(sock);
                    Invoke(vs);
                }
                else
                {
                    lblConnectStatus.Text = "Unable to connect to remote machine, Connect Failed!";
                    PublicMethods.logToDB("Unexpected error in connection to the network", "Unexpected error in connection to the network", installationID);
                }
			}
			catch( Exception ex )
			{
                ///threadSafe!
				lblConnectStatus.Text = "Unusual error during Connection!" ;
                PublicMethods.logToDB("Unexpected error in connection to the network", ex.ToString(),installationID);
			}
		}
        private void VisibilityControl()
        {
            btnSend.Visible = true;
            btnConnect.Enabled = false;
            lblConnectStatus.Text = "Connected!";
        }
		/// <summary> 
		/// Note: If not data was recieved the connection has probably 
		/// died.
		/// </summary>
		/// <param name="ar"></param>
		public void OnReceivedData( IAsyncResult ar )
		{
			// Socket was the passed in object
            Socket sock = (Socket)ar.AsyncState;

			// Check if we got any data
			try
			{
				int nBytesRec = sock.EndReceive( ar );
				if( nBytesRec > 0 )
				{
					// Wrote the data to the List
					string sRecieved = Encoding.ASCII.GetString( m_byBuff, 0, nBytesRec );

                    // . Invoke is thread safe
					Invoke(m_AddMessage, new string [] { sRecieved } );
                    //lstRecievedData.Items.Add(sRecieved);//NOT THREAD SAFE!

					// If the connection is still usable restablish the callback
					SetupRecieveCallback( sock );
				}
				else
				{
					// If no data was recieved then the connection is probably dead
                    if (send("STOP\r"))
                    {
                        Console.WriteLine("Client {0}, disconnected", sock.RemoteEndPoint);
                    }
                    else
                    {
                        Console.WriteLine("Can not send stop command!");
                    }
				}
			}
			catch( Exception ex )
			{
				lblConnectStatus.Text = "Unusual error druing recieve!" ;
                PublicMethods.logToDB("Unexpected error on receiving packets", ex.ToString(), installationID);
			}
		}

        /// <summary>
        /// The threadsafe method to add recieved data to the local buffer
        /// </summary>
        /// <param name="sMessage"></param>
		public void OnAddMessage( string sMessage )
		{
            ////bayad inja bashe ke threadsafe bashe! test badddi
            if (chShow.Checked )
            {
                textBox1.Text += sMessage;
                textBox2.Text += "\r\n" + sMessage;
                XMLReceieved += sMessage;
            }
            if (XMLReceieved.EndsWith("</measurz>\r\n"))
            {
                Parser.Parse(XMLReceieved,installationID, lastResetTime);
                ClickDisconnect();
            }
            
            //Parser.Parse(sMessage, installationID);
            //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Users\Mahdokht-A\Desktop\Data.txt", true))
            //{
            //    file.WriteLine(sMessage + "@ " + System.DateTime.Now);
            //} 
		}

		/// <summary>
		/// Setup the callback for recieved data and loss of conneciton
		/// </summary>
		public void SetupRecieveCallback( Socket sock )
		{
			try
			{
				AsyncCallback recieveData = new AsyncCallback( OnReceivedData );
				sock.BeginReceive( m_byBuff, 0, m_byBuff.Length, SocketFlags.None, recieveData, sock );
			}
			catch( Exception ex )
			{
				MessageBox.Show( this, ex.Message, "Setup Recieve Callback failed!" );
                PublicMethods.logToDB("Unexpected error recieve data", ex.ToString(), installationID);
			}
		}

		/// <summary>
		/// Close the Socket connection bofore going home
		/// </summary>
		private void FormMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if( m_sock != null && m_sock.Connected )
			{
				m_sock.Shutdown( SocketShutdown.Both );
				m_sock.Close();
			}
		}

		/// <summary>
		/// Send the Message in the Message area. Only do this if we are connected
		/// </summary>
		//private void m_btnSend_Click(object sender, System.EventArgs e)
        public void ClickSend()
		{
            if (!send("SENDDATA\r"))
            {
                MessageBox.Show("Send Message Failed!");
                PublicMethods.logToDB("Send message faild", "Send \"SENDDATA\\r\" Message Faild", installationID);
            }
		}

        private bool send(String requestMsg)
        {
            bool result = false;

            // Check we are connected
            if (m_sock == null || !m_sock.Connected)
            {
                lblConnectStatus.Text = "Not Connected!";
            }

            try
            {
                // Convert to byte array and send.
                byte[] byteDateLine = Encoding.ASCII.GetBytes(requestMsg);
                m_sock.Send(byteDateLine, byteDateLine.Length, 0);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                PublicMethods.logToDB("An unexpected error in sending msg", ex.ToString(), installationID);
			}
            return result;
        }

        public bool ClickDisconnect()
        {
            bool isStoped = false;
            if (!send("STOP\r"))
            {
                MessageBox.Show("Send Stop\r Message Failed!");
                PublicMethods.logToDB("Send message faild", "Send \"STOP\\r\" Message Faild", installationID);
                isStoped = false;
            }
            else
            {
                m_sock.Shutdown(SocketShutdown.Both);
                m_sock.Close();
                isStoped = true;
            }
            return isStoped;
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            ClickConnect("192.168.4.3", 2222);
            System.Threading.Thread.Sleep(70);
            ClickSend();
            //System.Threading.Thread.Sleep(1000);
            
        }
	}
}
