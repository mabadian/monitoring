﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Configuration;
namespace Monitoring
{
    class PublicMethods
    {
        //logToFile("lostPacketLog", lostPacket, installationId);

        static MySql.Data.MySqlClient.MySqlConnection _myconn;
        static MySql.Data.MySqlClient.MySqlCommand _command;
        static string _connectionStr = ConfigurationManager.ConnectionStrings["MyDBConnection"].ConnectionString;

        public static void logToDB(string errorMsg, string errorDesc, string installationId)
        {
            try
            {
                _myconn = new MySql.Data.MySqlClient.MySqlConnection(_connectionStr);

                _myconn.Open();
                string query = "INSERT INTO log_error(`installationID`, `time`,`error_message`, `error_detail`)VALUES('" + installationId + "', '" + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "', '" + errorMsg +  "', '" + errorDesc + "' )";

                _command = new MySql.Data.MySqlClient.MySqlCommand(query, _myconn);
                _command.ExecuteNonQuery();
                _command.Dispose();

            }
            catch (System.Exception exeption)
            {
                logToFile("errorLog", errorDesc, installationId);
                logToFile("errorLog", exeption.ToString(), installationId);
            }
            finally
            {
                if (_myconn.State != System.Data.ConnectionState.Closed)
                { _myconn.Close(); }
                _myconn.Dispose();
            }

        }

        public static void logToFile(string fileName, string errorDesc, string installationId)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Users\Mahdokht-A\Desktop\"+fileName +".txt", true))
            {
                file.WriteLine(errorDesc + " @ " +System.DateTime.Now.ToString() + " installationId: " +installationId);
            }
        }

        public static void sendTxtMsg(string ErrArea, string ErrorDesc, string installationId, string emailAddress)
        {
            MailMessage theMailMessage = new MailMessage("errornotification@jlmenergyinc.com", "9167280466@mymetropcs.com");
            try
            {
                theMailMessage.Body = ErrorDesc;
                theMailMessage.Subject = "An unexpected error accured in "+ ErrArea;  

                //E-Mail Credentials and Sending
                SmtpClient theClient = new SmtpClient("mail.jlmenergyinc.com");
                System.Net.NetworkCredential theCredential = new
                System.Net.NetworkCredential("errornotification@jlmenergyinc.com", "A5&6Gs9*q%qx");
                theClient.Credentials = theCredential;
                theClient.Send(theMailMessage);
            }
            catch (System.Exception ex)
            {
                logToDB("send text msg faild!", ex.ToString(), installationId);
            }
        }

        public static void logLostPacketToDB(string lostPacket, string installationId)
        {
            try
            {
                _myconn = new MySql.Data.MySqlClient.MySqlConnection(_connectionStr);
                
                _myconn.Open();

                string query = "INSERT INTO log_lost_packets(`idinstallation_data`,`time`,`lostPacket`)VALUES('" + installationId + "', '" + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "', '" + lostPacket + "')";

                _command = new MySql.Data.MySqlClient.MySqlCommand(query, _myconn);
                _command.ExecuteNonQuery();
                _command.Dispose();

            }
            catch (System.Exception e)
            {
                logToFile("errorLog", e.ToString(), installationId);
                logToFile("lostPacketLog", lostPacket, installationId);
            }
            finally
            {
                if (_myconn.State != System.Data.ConnectionState.Closed)
                { _myconn.Close(); }
                _myconn.Dispose();
            }
        }
    }
}
