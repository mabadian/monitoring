﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;
using System.Text.RegularExpressions;

namespace Monitoring
{
    public static class Parser
    {
        static string xmlStr;
        static string installationID;
        static System.DateTime lastResetTime;
        static InstallationData ins = new InstallationData("111", lastResetTime);
        public static void Parse(string _xmlString, string _installID, System.DateTime _lastResetTime)
        {
            xmlStr = XMLUtility.RemoveWhitespace(_xmlString);
            installationID = _installID;
            lastResetTime = _lastResetTime;
            bool isValid = false;
            bool isValidPacket = false;
            isValidPacket = (xmlStr != "") && (IsValidXML(xmlStr)) && (isValidPack(xmlStr));
            if(isValidPacket)
            {
                InstallationData ins = new InstallationData(installationID, lastResetTime);

                //            @"<measurz>
                //            <sgb time='11:59' ws='24' v='36' i='20' ilim='20' />
                //                <nodes>
                //                    <node sn='(100)00000083' >
                //                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                //                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                //                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                //                    </node>
                //                    <node sn='serial_number_n+1' >
                //                        <data seq='0' vin='11' vout='111' v='1' i='1' st='OK'/>
                //                        <data seq='1' vin='22' vout='222' v='2' i='2' st='This has folowing error'/>
                //                        <data seq='59' vin='59' vout='599' v='59' i='59' st='status'/>
                //                    </node>
                //                </nodes>
                //            </measurz>";

                using (XmlReader reader = XmlReader.Create(new StringReader(xmlStr)))
                {
                    isValid= reader.IsStartElement("measurz");

                    while (reader.Read() && reader.NodeType == XmlNodeType.Element && reader.Name == "sgb")
                    {

                        #region sgb
                        //if (reader.Name =="sgb")
                        //{
                            if (reader.MoveToFirstAttribute())
                            { 
                                if (reader.Name == "time")
                                {
                                    try
                                    {
                                        ins.SGBTimer = long.Parse(reader.Value);
                                    }
                                    catch (Exception e)
                                    {
                                        ins.SGBTimer = -100;
                                        isValid = false;
                                        //PublicMethods.logToDB("An unexpected error accured in parsing packet WindSpeed value is not valid. WinSpeed has the invalid value of" + reader.Value , e.ToString(), installationID);
                                        PublicMethods.logLostPacketToDB("SGBTimer with invalid value: " + reader.ReadContentAsString() + " is sent @ " + System.DateTime.Now.Date.ToString("yyyy/MM/dd") + " " + ins.SGBTimer.ToString(), installationID);
                                    }
                                }
                                else
                                {
                                    isValid = false;
                                }

                                if (reader.MoveToNextAttribute() && reader.Name == "ws")
                                {
                                    try
                                    {
                                        ins.WindSpeed = double.Parse(reader.Value);
                                    }
                                    catch (Exception e)
                                    {
                                        ins.WindSpeed = -100;
                                        isValid = false;
                                        //PublicMethods.logToDB("An unexpected error accured in parsing packet WindSpeed value is not valid. WinSpeed has the invalid value of" + reader.Value , e.ToString(), installationID);
                                        PublicMethods.logLostPacketToDB("WindSpeed with invalid value: " + reader.ReadContentAsString() + " is sent @ " + System.DateTime.Now.Date.ToString("yyyy/MM/dd") + " " + ins.SGBTimer.ToString(), installationID);
                                    }
                                }
                                else
                                {
                                    ins.WindSpeed = -100;
                                    isValid = false;
                                }

                                //v='voltage' i='current' ilim='set_current' />
                                if (reader.MoveToNextAttribute() && reader.Name == "v")
                                {
                                    try
                                    {
                                        ins.Voltage = double.Parse(reader.Value);

                                    }
                                    catch (Exception e)
                                    {
                                        ins.Voltage = -100;
                                        isValid = false;
                                        //PublicMethods.logToDB("An unexpected error accured in parsing installationPacket voltage value is not valid", e.ToString(), installationID);
                                        PublicMethods.logLostPacketToDB("Voltage with invalid value: " + reader.ReadContentAsString() + " is sent @ " + System.DateTime.Now.Date.ToString("yyyy/MM/dd") + " " + ins.SGBTimer.ToString(), installationID);
                                    }
                                }
                                else
                                {
                                    isValid = false;
                                }

                                if (reader.MoveToNextAttribute() && reader.Name == "i")
                                {
                                    try
                                    {
                                        ins.Current = double.Parse(reader.Value);

                                    }
                                    catch (Exception e)
                                    {
                                        ins.Current = -100;
                                        isValid = false;
                                        //PublicMethods.logToDB("An unexpected error accured in parsing installationPacket current value is not valid", e.ToString(), installationID);
                                        PublicMethods.logLostPacketToDB("Current with invalid value: " + reader.ReadContentAsString() + " is sent @ " + System.DateTime.Now.Date.ToString() + ins.SGBTimer.ToString(), installationID);
                                    }
                                }

                                else
                                {
                                    isValid = false;
                                }

                                if (reader.MoveToNextAttribute() && reader.Name == "ilim")
                                {
                                    try
                                    {
                                        ins.Ilim = double.Parse(reader.Value);

                                    }
                                    catch (Exception e)
                                    {
                                        ins.Ilim = -100;
                                        isValid = false;
                                        //PublicMethods.logToDB("An unexpected error accured in parsing installationPacket ilim value is not valid", e.ToString(), installationID);
                                        PublicMethods.logLostPacketToDB("ILIM with invalid value: " + reader.ReadContentAsString() + " is sent @ " + System.DateTime.Now.Date.ToString() + ins.SGBTimer.ToString(), installationID);
                                    }
                                }
                                else
                                {
                                    ///HERE NEEDED TO ADD to the lost packet & SAVE THE ENTIRE LINE OF data tag intor lost packet
                                    ///OR ??
                                    ///just say what is lost!
                                }
                            }
                        //}
                        #endregion

                        if (isValidPacket == true)
                        {
                            ins.Insert();
                        }
                    }
                    string serialNum;

                    while (reader.Read() && reader.NodeType == XmlNodeType.Element )
                    {
                        if (reader.Name != "")
                        {
                            if (reader.Name == "node" )
                            {
                                if (reader.HasAttributes && reader.MoveToNextAttribute() && reader.Name == "sn")
                                {
                                    //nodeCount index start from 0. it is still 0 and not added. 
                                    //Test for any type of error if value was not valid.
                                    serialNum = reader.Value;
                                
                                    while(reader.Read() &&  reader.Name !="" && reader.NodeType != XmlNodeType.EndElement && reader.Name == "data")
                                    {
                                        TurbineData trbn = new TurbineData(serialNum);

                                        //here do the time & make it time of installation OR do it when we want to add it????!!!
                                        trbn.Time = System.DateTime.Now;
                                        trbn.InstallationId = ins.lastIdAdded;


                                        #region Add installation Turbines
                                        if (reader.MoveToNextAttribute() && reader.Name == "seq")
                                        {
                                            try
                                            {
                                                trbn.Sec = int.Parse(reader.Value);
                                            }
                                            catch (Exception e)
                                            {
                                                //????????????????????????????
                                                //MAKE SURE THE data are in order
                                                //OR 
                                                //-100
                                                trbn.Sec = System.DateTime.Now.Millisecond;
                                                isValid = false;
                                                //PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket Sec value is not valid", e.ToString(), installationID);
                                                PublicMethods.logLostPacketToDB("<data seq='" + reader.Value + "... />", installationID);
                                                //OR
                                                //PublicMethods.logLostPacketToDB(xmlStr, installationID);

                                                }
                                        }
                                        else
                                        {
                                            trbn.Sec = -100;
                                            isValid = false;
                                            //PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket i value is not valid", "The xml packet does not contain st attribute in " + trbn.SerialNumber + " time: " + trbn.Time + " seq: " + trbn.Sec, installationID);
                                            PublicMethods.logLostPacketToDB("Turbine Serial Number: " + trbn.SerialNumber + " Sent @: " + trbn.Time + " InstallationId: " + trbn.InstallationId + " Seq Attribute in data element does not exist ", installationID);
                                        }

                                        if (reader.MoveToNextAttribute() && reader.Name == "vin")
                                        {
                                            try
                                            {
                                                trbn.VIn = int.Parse(reader.Value);
                                            }
                                            catch (Exception e)
                                            {
                                                isValid = false;
                                                trbn.VIn = -100;
                                                //PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket vin value is not valid", e.ToString(), installationID);
                                                PublicMethods.logLostPacketToDB("<data seq='" + trbn.Sec + "' vin='" + reader.Value + "... />", installationID);
                                                //OR
                                                //PublicMethods.logLostPacketToDB(xmlStr, installationID);
                                            }
                                        }
                                        else
                                        {
                                            trbn.VIn = -100;
                                            isValid = false;
                                            //PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket i value is not valid", "The xml packet does not contain st attribute in " + trbn.SerialNumber + " time: " + trbn.Time + " seq: " + trbn.Sec, installationID);
                                            PublicMethods.logLostPacketToDB("Turbine Serial Number: " + trbn.SerialNumber + " Sent @: " + trbn.Time.ToString() + " InstallationId: " + trbn.InstallationId + " Vin Attribute in data element does not exist ", installationID);
                                        }

                                        if (reader.MoveToNextAttribute() && reader.Name == "vout")
                                        {
                                            try
                                            {
                                                trbn.VOut = int.Parse(reader.Value);
                                            }
                                            catch (Exception e)
                                            {
                                                isValid = false;
                                                trbn.VOut = -100;
                                                //PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket vOut value is not valid", e.ToString(), installationID);
                                                PublicMethods.logLostPacketToDB("<data seq='" + trbn.Sec + "' vin='" + trbn.VIn.ToString() + "' vout='" + reader.Value + "' ... />", installationID);
                                                //OR
                                                //PublicMethods.logLostPacketToDB(xmlStr, installationID);
                                            }
                                        }
                                        else
                                        {
                                            isValid = false;
                                            trbn.VOut = -100;
                                            //PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket i value is not valid", "The xml packet does not contain st attribute in " + trbn.SerialNumber + " time: " + trbn.Time + " seq: " + trbn.Sec, installationID);
                                            PublicMethods.logLostPacketToDB("Turbine Serial Number: " + trbn.SerialNumber + " Sent @: " + trbn.Time.ToString() + " InstallationId: " + trbn.InstallationId + " Vout Attribute in data element does not exist ", installationID);
                                        }

                                        if (reader.MoveToNextAttribute() && reader.Name == "vb")
                                        {
                                            try
                                            {
                                                trbn.VBattery = int.Parse(reader.Value);
                                            }
                                            catch (Exception e)
                                            {
                                                isValid = false;
                                                trbn.VBattery = -100;
                                                //PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket v value is not valid", e.ToString(), installationID);
                                                PublicMethods.logLostPacketToDB("<data seq='" + trbn.Sec + "' vin='" + trbn.VIn.ToString() +"' vout='" + trbn.VOut + "' vb='" + reader.Value + "' ... />" , installationID);
                                                //OR
                                                //PublicMethods.logLostPacketToDB(xmlStr, installationID);
                                            }
                                        }
                                        else
                                        {
                                            isValid = false;
                                            trbn.VBattery = -100;
                                            //PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket i value is not valid", "The xml packet does not contain st attribute in " + trbn.SerialNumber + " time: " + trbn.Time + " seq: " + trbn.Sec, installationID);
                                            PublicMethods.logLostPacketToDB("Turbine Serial Number: " + trbn.SerialNumber + " Sent @: " + trbn.Time + " InstallationId: " + trbn.InstallationId + " VBattery Attribute in data element does not exist ", installationID);
                                        }

                                        if (reader.MoveToNextAttribute() && reader.Name == "i")
                                        {
                                            try
                                            {
                                                trbn.Current = int.Parse(reader.Value);
                                            }
                                            catch (Exception e)
                                            {
                                                isValid = false;
                                                trbn.Current = -100;
                                                //PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket i value is not valid", e.ToString(), installationID);
                                                PublicMethods.logLostPacketToDB("<data seq='" + trbn.Sec + "' vin='" + trbn.VIn.ToString() + "' vout='" + trbn.VOut + "' vb='" + trbn.VBattery + "' i='" + reader.Value + "' ... />", installationID);
                                                //OR
                                                //PublicMethods.logLostPacketToDB(xmlStr, installationID);
                                            }
                                        }
                                        else
                                        {
                                            isValid = false;
                                            trbn.Current = -100;
                                            //PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket i value is not valid", "The xml packet does not contain st attribute in " + trbn.SerialNumber + " time: " + trbn.Time + " seq: " + trbn.Sec, installationID);
                                            PublicMethods.logLostPacketToDB("Turbine Serial Number: " + trbn.SerialNumber + " Sent @: " + trbn.Time + " InstallationId: " + trbn.InstallationId + " i Attribute in data element does not exist ", installationID);
                                        }

                                        if (reader.MoveToNextAttribute() && reader.Name == "st")
                                        {
                                            trbn.Status = reader.Value;
                                        }
                                else
                                        {
                                            isValid = false;
                                            trbn.Status = "-100";
                                            //PublicMethods.logToDB("An unexpected error accured in parsing TurbinePacket i value is not valid", "The xml packet does not contain st attribute in " + trbn.SerialNumber + " time: " + trbn.Time + " seq: " + trbn.Sec, installationID);
                                            PublicMethods.logLostPacketToDB("Turbine Serial Number: " + trbn.SerialNumber + " Sent @: " + trbn.Time + " InstallationId: " + trbn.InstallationId + " Status Attribute in data element does not exist ", installationID);
                                        }
                                        #endregion

                                        //HAME KAR RO ANJAM BEDE!!!!!!!!!
                                        if (isValidPacket)
                                        {
                                            trbn.Insert();
                                        }
                                    }   
                                } 
                             }
                        }
                    }
                }
            }
        }
            
        //This is method is public just for testing. it can be private after test
        /// <summary>
        /// check if the string is wellFormatt xml
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        public static bool IsValidXML(string xmlString)
        {
            bool _isValid = false;
            try
            {
                using (XmlReader reader = XmlReader.Create(new StringReader(xmlString)))
                {
                    while (reader.Read())
                    {
                        _isValid = true;
                    }
                }
            }
            catch (XmlException e)
            {
                _isValid = false;
                PublicMethods.logLostPacketToDB(xmlStr, installationID);
            }
            return _isValid;
        }

    /// <summary>
    /// check if the string is valid Packet
    /// </summary>
    /// <param name="packet"></param>
    /// <returns></returns>
    public static bool isValidPack(string packet)
    {
        bool _isValid = false;

        //?????????? 
        //shall i use this or keep it in the parser?!
        //just for the sake of not being blocked in the parser class
        _isValid = true;

        return _isValid;
    }
}
        public static class XMLUtility
        {
            public static string RemoveWhitespace(string xml)
            {
                Regex regex = new Regex(@">\s*<");
                xml = regex.Replace(xml, "><");

                return xml.Trim();
            }
        }
    }

