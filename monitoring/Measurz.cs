﻿using MySql;
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace MonitoringUnit
{
    public class Measurz
    {
        public string xmlStr;
        public int lastIdAdded;
        public bool _complete = false;
        public bool _reseted = false;
        private List<TurbineData> _turbineList = new List<TurbineData>();
        private List<InstallationData> _installList = new List<InstallationData>();
        private string _connectionStr = ConfigurationManager.ConnectionStrings["MntDBConnection"].ConnectionString;
        //private Dictionary<long, Values> _timerDictionary = new Dictionary<long, Values>();
        private Dictionary<long, DateTime> _timerDictionary = new Dictionary<long, DateTime>();

        /// <summary>
        /// As a foreign key of the primary key ID auto incriment field in Installarioin_data table
        /// </summary>
        public List<InstallationData> instalList
        {
            get
            {
                return _installList;
            }
            set
            {
                _installList = value;
            }
        }

        public List<TurbineData> turbineList
        {
            get
            {
                return _turbineList;
            }
            set
            {
                _turbineList = value;
            }
        }

        public bool complete
        {
            get
            {
                return _complete;
            }
            set
            {
                _complete = value;
            }
        }

        //private bool CreatDictionary()
        //{
        //    bool result = false;
        //    _timerDictionary = new Dictionary<long, Values>();
        //    _timerDictionary.Add(instalList[]

        //    return result;
        //}


        public bool Insert()
        {
            bool blnResult = true;
            MySqlConnection _myconn = new MySqlConnection(_connectionStr);
            MySqlCommand _command;
            int i = 0;
            try
            {
                //dbConnection.OpenConnection();
                _myconn.Open();
                for (i = 0; i < instalList.Count; i++)
                {
                    instalList[i].sGBTime = instalList[i].resetTime.AddSeconds(instalList[i].sGBTimer);
                    //if (i > 0 && instalList[i].sGBTimer == instalList[i - 1].sGBTimer)
                    //{

                        
                    string query = "insert into zefr_installation_data (`installation_serialnumber`,`time`,`v`,`i`,`ws`,`ilim`) VALUES( '" + instalList[i].installationID + "' ,'" + instalList[i].sGBTime.ToString("yyyy/MM/dd HH:mm:ss") + "', " + instalList[i].voltage + ", " + instalList[i].current + ", " + instalList[i].windSpeed + ", " + instalList[i].ilim + ")";
                    _command = new MySqlCommand(query, _myconn);
                    //Test : is its adding / commiting or not!
                    _command.ExecuteNonQuery();
                    _command = new MySqlCommand("SELECT LAST_INSERT_ID() FROM zefr_installation_data", _myconn);
                    instalList[i].dbIdAdded = int.Parse(_command.ExecuteScalar().ToString());
                    //_timerDictionary.Add(instalList[i].sGBTimer, new Values(instalList[i].sGBTime, instalList[i].dbIdAdded));
                    if ((i == 0) || (i > 0 && instalList[i].sGBTimer != instalList[i - 1].sGBTimer))
                    {
                        _timerDictionary.Add(instalList[i].sGBTimer, instalList[i].sGBTime);
                    }
                    else
                    {
                        //PublicMethods.logLostPacketToDB(instalList[i].ToString(), instalList[i].installationID);
                    }
                }

                for (int j = 0; j < turbineList.Count; j++)
                {
                    //turbineList[j].installationId = _timerDictionary[turbineList[j].nodeTiemr].dbAddedIdValue;
                    if (turbineList[j].seq != -100)
                    {
                        //turbineList[j].time = _timerDictionary[turbineList[j].nodeTiemr].timeValue.AddSeconds(-60 + turbineList[j].seq);
                        turbineList[j].time = _timerDictionary[turbineList[j].nodeTiemr].AddSeconds(-60 + turbineList[j].seq);
                    }
                    else
                    {
                        //?????????
                    }

                    string query = "insert into zefr_node_data (`installation_id`,`serial_number`,`time`,`v_in`,`v_out`,`v_battery`, `status`) VALUES( " + turbineList[j].installationId.ToString() + " ,'" + turbineList[j].serialNumber + "' ,'" + turbineList[j].time.ToString("yyyy/MM/dd HH:mm:ss") + "', " + turbineList[j].vIn.ToString() + ", " + turbineList[j].vOut.ToString() + ", " + turbineList[j].vBattery.ToString() + ",'" + turbineList[j].status + "' )";
                    _command = new MySqlCommand(query, _myconn);

                    //Test : is its adding / commiting or not!
                    _command.ExecuteNonQuery();
                    _command.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                blnResult = false;
                PublicMethods.logLostPacketToDB("First sgbTime: " + instalList[0].sGBTime.ToString("yyyy/MM/dd HH:mm:ss") + xmlStr, instalList[0].installationID);
                PublicMethods.logToDB("Exception in inserting Measurz", ex.ToString(), instalList[i].installationID);
            }
            finally
            {
                if (_myconn.State != System.Data.ConnectionState.Closed)
                    _myconn.Close();
                _myconn.Dispose();
            }
            return (blnResult);
        }
    }

    public class Values
    {
        public Values(DateTime sgbTimer, Int64 lastAddedId)
        {
            timeValue = sgbTimer;
            dbAddedIdValue = lastAddedId;
        }
        private DateTime _timeValue;
        private Int64 _dbAddedIdValue;

        public DateTime timeValue
        {
            get
            {
                return _timeValue;
            }
            set
            {
                _timeValue = value;
            }
        }

        public Int64 dbAddedIdValue
        {
            get
            {
                return _dbAddedIdValue;
            }
            set
            {
                _dbAddedIdValue = value;
            }
        }
    }

}
