﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Net;					
using System.Net.Sockets;			 
using MySql;
using System.Data.SqlClient;	

namespace Monitoring
{
    // Declare the delegate prototype to send data back to the form
    delegate void AddMessage(string sNewMessage);
    class GetSGBData
    {
        string errorMsg = "";
        string XMLReceieved = "";
        string installationID = "";
        System.DateTime lastResetTime;
        private Socket m_sock;
        private byte[] m_byBuff = new byte[256];
        private event AddMessage m_AddMessage;

        public GetSGBData(String _installationID, System.DateTime _lastResetTime)
		{
            lastResetTime = _lastResetTime;
            installationID = _installationID;

			// Add Message Event handler for Form decoupling from input thread
            m_AddMessage = new AddMessage(OnAddMessage);
		}

        //private void btnConnect_Click(object sender, System.EventArgs e)
        public void ClickConnect(string ipAddress, int port)
        {
            try
            {
                // Close the socket if it is still open
                if (m_sock != null && m_sock.Connected)
                {
                    ClickDisconnect();
                    //m_sock.Shutdown(SocketShutdown.Both);
                    //System.Threading.Thread.Sleep(10);
                    //m_sock.Close();
                    //m_sock.Disconnect(true);
                    //if connection is open, diconnect it first and start after
                    // THIS NEEDS TO BE DONE AFTER PETER fixed the dico
                    //byte[] byteDateLine = Encoding.ASCII.GetBytes("DISCONNECT\r");
                    //m_sock.Send(byteDateLine, byteDateLine.Length, 0);
                }

                m_sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                // Define the Server address and port
                //The IP & port Comming from Database in phase2
                IPEndPoint epServer = new IPEndPoint(IPAddress.Parse(ipAddress), port);

                // Connect to the server blocking method and setup callback for recieved data
                m_sock.Connect(epServer);

                SetupRecieveCallback(m_sock);

                // Connect to server non-Blocking method
                //m_sock.Blocking = false;
                //AsyncCallback onconnect = new AsyncCallback(OnConnect);
                //m_sock.BeginConnect(epServer, onconnect, m_sock);
            }
            catch (Exception ex)
            {
                errorMsg = "Server Connect failed!";
                Console.WriteLine(ex.Message);
                PublicMethods.logToDB("An unexpected error accured in Connection to the network", ex.ToString(), installationID);
            }
        }
        /// <summary>
        /// the method is being called in the conn
        /// </summary>
        /// <param name="ar"></param>
        public void OnConnect(IAsyncResult ar)
        {
            Socket sock = (Socket)ar.AsyncState;
            try
            {
                if (sock.Connected)
                {
                    SetupRecieveCallback(sock);
                }
                else
                {
                    errorMsg= "Unable to connect to remote machine, Connect Failed!";
                    PublicMethods.logToDB("Unexpected error in connection to the network", "Unexpected error in connection to the network", installationID);
                }
            }
            catch (Exception ex)
            {
                ///threadSafe!
                ClickDisconnect();
                errorMsg = "Unusual error during Connection!";
                PublicMethods.logToDB("Unexpected error in connection to the network", ex.ToString(), installationID);
            }
        }

        /// <summary> 
        /// Note: If not data was recieved the connection has probably 
        /// died.
        /// </summary>
        /// <param name="ar"></param>
        public void OnReceivedData(IAsyncResult ar)
        {
            // Socket was the passed in object
            Socket sock = (Socket)ar.AsyncState;

            // Check if we got any data
            try
            {
                int nBytesRec = sock.EndReceive(ar);
                if (nBytesRec > 0)
                {
                    // Wrote the data to the List
                    string sRecieved = Encoding.ASCII.GetString(m_byBuff, 0, nBytesRec);

                    // . Invoke is thread safe
                    //if (XMLReceieved.EndsWith("</measurz>"))
                    //{
                    m_AddMessage.Invoke(sRecieved);
                        //XMLReceieved += sRecieved ;
                    
                        //{
                            //Parser.Parse(XMLReceieved, installationID);
                            //ClickDisconnect();
                        //}
                        //else
                        //{
                            //Invoke(m_AddMessage, new string[] { sRecieved });
                            //lstRecievedData.Items.Add(sRecieved);//NOT THREAD SAFE!

                            // If the connection is still usable restablish the callback
                    if(m_sock!= null && m_sock.Connected)
                    {
                        SetupRecieveCallback(sock);
                    }
                }
                else
                {
                    // If no data was recieved then the connection is probably dead
                    if (ClickDisconnect())
                    {
                        Console.WriteLine("Client {0}, disconnected", sock.RemoteEndPoint);
                    }
                    else
                    {
                        Console.WriteLine("Can not send stop command!");
                    }
                }
            }
            catch (Exception ex)
            {
                ClickDisconnect();
                errorMsg= "Unusual error druing recieve!";
                PublicMethods.logToDB("Unexpected error on receiving packets", ex.ToString(), installationID);
            }
        }

        /// <summary>
        /// The threadsafe method to add recieved data to the local buffer
        /// </summary>
        /// <param name="sMessage"></param>
        public void OnAddMessage(string sMessage)
        {
           
            ////bayad inja bashe ke threadsafe bashe! test badddi
            //if (chShow.Checked )
            //{
            //textBox1.Text += sMessage;
            //textBox2.Text += "\r\n" + sMessage;
            XMLReceieved += sMessage;
            //}
            if (XMLReceieved.EndsWith("</measurz>"))
            {
                Parser.Parse(XMLReceieved, installationID, lastResetTime);
                ClickDisconnect();
            }

            //Parser.Parse(sMessage, installationID);
            //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Users\Mahdokht-A\Desktop\Data.txt", true))
            //{
            //    file.WriteLine(sMessage + "@ " + System.DateTime.Now);
            //} 
        }

        /// <summary>
        /// Setup the callback for recieved data and loss of conneciton
        /// </summary>
        public void SetupRecieveCallback(Socket sock)
        {
            try
            {
                AsyncCallback recieveData = new AsyncCallback(OnReceivedData);
                sock.BeginReceive(m_byBuff, 0, m_byBuff.Length, SocketFlags.None, recieveData, sock);
            }
            catch (Exception ex)
            {
                //ClickDisconnect();
                Console.WriteLine(ex.Message + "Setup Recieve Callback failed!");
                //MessageBox.Show(this, ex.Message, "Setup Recieve Callback failed!");
                PublicMethods.logToDB("Unexpected error recieve data", ex.ToString(), installationID);
            }
        }
        /// <summary>
        /// Send the Message in the Message area. Only do this if we are connected
        /// </summary>
        //private void m_btnSend_Click(object sender, System.EventArgs e)
        public void ClickSend()
        {
            if (!send("SENDDATA\r"))
            {
                Console.WriteLine("SENDDATA Message Faild");
                //MessageBox.Show("Send Message Failed!");
                //PublicMethods.logToDB("Send message faild", "Send SENDDATA Message Faild", installationID);
            }
        }

        private bool send(String requestMsg)
        {
            bool result = false;

            // Check we are connected
            if (m_sock == null || !m_sock.Connected)
            {
                errorMsg = "sending "+ requestMsg +" faild because Not Connected!";
                Console.WriteLine(errorMsg);
                Console.WriteLine("Not connected");
            }

            try
            {
                // Convert to byte array and send.
                byte[] byteDateLine = Encoding.ASCII.GetBytes(requestMsg);
                m_sock.Send(byteDateLine, byteDateLine.Length, 0);
                result = true;
            }
            catch (Exception ex)
            {
                //ClickDisconnect();
                Console.WriteLine(ex.Message);
                result = false;
                PublicMethods.logToDB("An unexpected error in sending msg", ex.ToString(), installationID);
            }
            return result;
        }

        public bool ClickDisconnect()
        {
            bool isStoped = false;
            if (!send("STOP\r"))
            {
                //send("STOP\r");
                Console.WriteLine("STOP send faild.");
                //MessageBox.Show("Send Stop\r Message Failed!");
                PublicMethods.logToDB("Send message faild", "Send STOP Message Faild", installationID);
                isStoped = false;
            }
            else
            {
                System.Threading.Thread.Sleep(20);

                m_sock.Shutdown(SocketShutdown.Both);
                m_sock.Disconnect(true);
                m_sock.Close();
                
                isStoped = true;
            }
            return isStoped;
        }
    }
}
