﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Monitoring
{
    public partial class InstallationList : Form
    {
        public InstallationList()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            ////FIND THE ID from the database
            // lstInstallation.SelectedItem.ToString();

            FormMain frm = new FormMain("9999",System.DateTime.Now);//lstInstallation.SelectedItem.ToString());
            frm.Show();
            this.Hide();
        }

        private void lstInstallation_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnOk.Enabled = true;
        }
    }
}
