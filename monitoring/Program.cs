﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;

namespace Monitoring
{
    class Program
    {
        //[STAThread]
        //static void Main()
        //{
        //    //Application.Run(new InstallationList());
        //}
        static int i = 0;
        static string dge = "";
        static InstallProp[] installPropList;
        static GetSGBData frm;

        public static void Main()
        {
            // Create a Timer object that knows to call our TimerCallback
            // method once every 2000 milliseconds.
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);
  
            while (dge.Length == 0)
            {
                Console.WriteLine("What DGE Number Do I have?");
                dge = Console.ReadLine();
            }

            System.Threading.Timer t = new System.Threading.Timer(TimerCallback, null, 0, 60000);
            // Wait for the user to hit <Enter>
            Console.ReadLine();
        }
         
        private static void TimerCallback(Object o)
        {
            i++;
            installPropList = InstallProp.GetPrperties(dge);
            //GetIPAddress(InstallationID);
            for (int j = 0; j < installPropList.Length ; j++)
            {
                //GetSGBData frm = new GetSGBData(installPropList[j].InstallationID);
                frm = new GetSGBData(installPropList[j].InstallationID, installPropList[j].ResetTime);
                frm.ClickConnect(installPropList[j].IpAddress, installPropList[j].Port);
                System.Threading.Thread.Sleep(70);
                frm.ClickSend();

                System.DateTime MyTime = System.DateTime.Now.AddMinutes(-60);
                Console.WriteLine(MyTime.ToString());
                // Display the date/time when this method got called.
                //Console.WriteLine("In TimerCallback: " + DateTime.Now);
                // Force a garbage collection to occur for this demo.
                
                //Console.ReadLine();
            }

            GC.Collect();
        }

        static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            frm.ClickDisconnect();
        }
    }
}
